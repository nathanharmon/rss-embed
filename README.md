# RSS Embed

Modify the RSS feed of video hosting sites to display the embedded video instead of the standard video player.

## Goal

When watching videos, it can be easy to get lost in the "recommended videos" section, watching video after video. This script acts as a middleman between the actual RSS feed and your feed reader. The result is an RSS feed that takes you to the embedded video player, which contains only your desired video and nothing else.

## Newsboat Filter Installation

1. Clone the repo.
2. Move 'rssfilter' to ~/.local/bin/
3. Add URLs into your newsboat config in the format

```
filter:~/.local/bin/rssfilter:<YOUR FEED URL HERE>
```

4. Refresh the feeds in your feed reader.

## Standalone Installation

1. Clone the repo.
2. Move 'rssembed' and 'rssfilter' to ~/.local/bin/
3. Add URLs to ~/.config/rssembed/urls
4. Run `$ rssembed`
5. Add ~/.cache/rssembed/<YOUR FEED HASH> to your RSS feed reader.
6. Refresh the feeds in your feed reader.

*By default, the script stores a feed cache for an hour. If you don't want feed caching, run `$ rssembed` with the -f option.*

You should now be able to open an entry with a video. The video now opens in an embedded player instead of the standard video player.

## For the curious: How `rssfilter` works on its own

rssfilter will take a feed URL as an argument and convert it as expected
```
rssfilter <URL>
```

or, rssfilter will read the raw feed from standard input and output the converted feed
```
echo $feed | rssfilter
```

## License

RSS Embed  Copyright (C) 2021  Nathan Harmon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
